variable "cluster_name" {
  description = "The separator to use between the prefix and the generated timestamp for resource names"
  type        = string
  default     = "self-nodes"
}

variable "cluster_version" {
  description = "The separator to use between the prefix and the generated timestamp for resource names"
  type        = string
  default     = "1.27"
}